package com.example;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class Lista {
	
	public static void main(String[] args) {
		
		Scanner ler = new Scanner(System.in);
		
		ArrayList<String> agenda = new ArrayList();

		agenda.add("Caio; 1111 - 1111");
		agenda.add("João; 2222 - 2222");
		agenda.add("Maria; 3333 - 3333");
		agenda.add("Ana; 4444 - 4444");
		
		int i ;
		
		System.out.printf("Percorrendo o ArrayList (usando o índice)\n");
	    int n = agenda.size();
	    for (i=0; i<n; i++) {
	      System.out.printf("Posição %d: %s\n", i, agenda.get(i));
	    }
	    
	    System.out.printf("\nInforme a posição a ser excluída:\n");
	    i = ler.nextInt();
	    
	    try {
	    	agenda.remove(i);
	    }catch(IndexOutOfBoundsException e) {
	    	
	    	System.out.printf("\nErro: Posição inválida. (%s)", e.getMessage());
	    }
	    
	    System.out.printf("\nPercorrendo o ArrayList (usando for-each)\n");
	    i = 0;
	    for (String contato: agenda) {
	      System.out.printf("Posição %d- %s\n", i, contato);
	      i++;
	    }
	
	
	
	
	
	}
	
	
		
}



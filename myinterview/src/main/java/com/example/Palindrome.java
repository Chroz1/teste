package com.example;

import javax.swing.JOptionPane;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class Palindrome {
	
	public static void main(String[]args) {
		
		String palavra, palavraInvertida = "";
		int tamanho, i;
		
		//Armazena a palavra numa String.
		palavra = JOptionPane.showInputDialog(null,"Digite uma palavra");
		
		//Tamanho recebe a quantidade de letras da palavra.
		tamanho = palavra.length();
		
		//Pega a ultima letra da variável palavra e inseri na primeira da variavel palavraInvertida, assim sucessivamente
		for(i=tamanho - 1; i >= 0; i --) {
			palavraInvertida += palavra.charAt(i);
			
		}
		
		//Compara as variaveis e exibe o resultado
		if(palavra.equals(palavraInvertida))
			JOptionPane.showMessageDialog(null,palavra + " É uma palavra palindroma" );
		else
			JOptionPane.showMessageDialog(null, palavra + " Não é uma palavra palindroma");
	}

}

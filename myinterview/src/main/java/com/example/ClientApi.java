package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */
public class ClientApi {
	
	
	public static void main(String[] args) throws IOException {


      String result = null;

      URL url = new URL("https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda");

      HttpURLConnection connectionAPI = (HttpURLConnection) url.openConnection();

      connectionAPI.setRequestMethod("GET");

      connectionAPI.connect();

      BufferedReader inputBuffer = new BufferedReader(new InputStreamReader(connectionAPI.getInputStream()));

      String line;

      StringBuilder builder = new StringBuilder();

      while ((line = inputBuffer.readLine()) != null) {

        builder.append(line);
      }

      inputBuffer.close();


      result = builder.toString();

      connectionAPI.disconnect();

  }
}	 